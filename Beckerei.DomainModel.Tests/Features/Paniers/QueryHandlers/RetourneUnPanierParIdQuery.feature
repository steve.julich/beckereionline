﻿Fonctionnalité: RetourneUnPanierParIdQuery

Plan du scénario: Demande de retourner un panier par son id
Etant donné un service de mapping entité vers dto
Et un faux repository de panier contenant les données suivantes
| ID | CLIENT  | NB_ARTICLE |
| 1  | Pierre  | 2          |
| 2  | Jacques | 250        |
Et un gestionnaire de requête du type RetourneUnPanierParIdQueryHandler
Quand on envoie une requête du type RetourneUnPanierParIdQuery avec un id '<ID>'
Alors on obtient '<NB_EXCEPTION>' exception du type '<TYPE_EXCEPTION>'
Et on obtient bien '<NB_PANIERS>' panier avec un id '<ID>' contenant '<NB_ARTICLES>' articles
Exemples:
| ID | NB_ARTICLES | NB_PANIERS | NB_EXCEPTION | TYPE_EXCEPTION |
| 1  | 2           | 1          | 0            |                |
| 2  | 250         | 1          | 0            |                |
| 3  |             | 0          | 1            |                |

Scénario: Demande de retourner tous les panier
Etant donné un service de mapping entité vers dto
Et un faux repository de panier contenant les données suivantes
| ID | CLIENT  | NB_ARTICLE |
| 1  | Pierre  | 2          |
| 2  | Jacques | 250        |
Et un gestionnaire de requête du type Retourne TousLesPaniers
Quand on envoie une requête du type RetourneTousLesPaniersQuery
Alors on obtient '0' exception du type ''
Et on obtient bien '2' panier
