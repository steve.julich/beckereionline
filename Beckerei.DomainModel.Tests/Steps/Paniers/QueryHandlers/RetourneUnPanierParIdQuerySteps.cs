﻿using AutoFixture;
using AutoMapper;
using Beckerei.DomainModel.Clients.Entities;
using Beckerei.DomainModel.Paniers.Entities;
using Beckerei.DomainModel.Paniers.Repositories;
using Beckerei.Dtos.Paniers;
using Beckerei.ReadDomainModel.Mappings;
using Beckerei.ReadDomainModel.Paniers.Queries;
using Beckerei.ReadDomainModel.Paniers.QueryHandlers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace Beckerei.DomainModel.Tests.Steps.Paniers.QueryHandlers
{
    [Binding]
    [Scope(Feature = "RetourneUnPanierParIdQuery")]
    public class RetourneUnPanierParIdQuerySteps
    {
        private IMapper _mapper;
        private IRepositoryPaniers _repoPaniers;
        private PanierDetailDto _panierResult;
        private List<PanierForListDto> _paniersResult;
        private RetourneUnPanierParIdQueryHandler _byIdHandler;
        private RetourneTousLesPaniersQueryHandler _tousHandler;
        private RetourneUnPanierParIdQuery _byIdQuery;
        private RetourneTousLesPaniersQuery _tousQuery;
        private Fixture fixture;
        private Exception _exceptionThrown;

        [BeforeScenario]
        public void BeforeScenario()
        {
            //On prépare
            fixture = new Fixture();
            //On ne râle pas en cas de référence circulaire (lié à EF)
            fixture.Behaviors.Remove(fixture.Behaviors.Single(b => b.GetType() == typeof(ThrowingRecursionBehavior)));
            //On ajoute le fait qu'on s'arrête en cas de référence circulaire
            fixture.Behaviors.Add(new OmitOnRecursionBehavior(2));
        }

        [AfterScenario]
        public void AfterScenario()
        {
            //On nettoie
            _repoPaniers = null;
            _mapper = null;
            _panierResult = null;
            fixture = null;
            _byIdHandler = null;
            _byIdQuery = null;
            _exceptionThrown = null;
            _paniersResult = null;
        }

        [Given(@"un service de mapping entité vers dto")]
        public void SoitUnServiceDeMappingEntiteVersDto()
        {
            var config = new MapperConfiguration(cfg => cfg.AddProfile<MapperProfile>());
            config.AssertConfigurationIsValid();
            _mapper = config.CreateMapper();
        }
        
        [Given(@"un faux repository de panier contenant les données suivantes")]
        public void SoitUnFauxRepositoryDePanierContenantLesDonneesSuivantes(Table table)
        {
            List<Panier> lstPaniers = new List<Panier>();
            foreach (var row in table.Rows)
            {
                lstPaniers.Add(new Panier
                {
                    Articles = fixture.Build<ArticleDansLePanier>()
                                        .CreateMany(int.Parse(row["NB_ARTICLE"])).ToList(),
                    Client = fixture.Build<Client>().With(c => c.Nom, row["CLIENT"]).Create(),
                    Id = int.Parse(row["ID"])
                });

            }
            var mock = new Mock<IRepositoryPaniers>();
            mock.Setup(r => r.GetPanierByIdAsync(It.IsAny<int>()))
                .Returns<int>((id) =>
                {
                    return Task.Run(() => lstPaniers.SingleOrDefault(p => p.Id == id));
                });
            mock.Setup(r => r.GetPanierAsync(It.IsAny<int?>(), It.IsAny<int?>()))
                .Returns<int?, int?>((top, skip) =>
                {
                    var tmp = lstPaniers;
                    if (top != null)
                        tmp.Take(top.Value);
                    if (skip != null)
                        tmp.Skip(skip.Value);
                    return Task.Run(() => lstPaniers);
                });
            _repoPaniers = mock.Object;
        }
        
        [Given(@"un gestionnaire de requête du type RetourneUnPanierParIdQueryHandler")]
        public void SoitUnGestionnaireDeRequeteDuTypeRetourneUnPanierParIdQueryHandler()
        {
            _byIdHandler = new RetourneUnPanierParIdQueryHandler(_repoPaniers, _mapper);
        }
        
        [When(@"on envoie une requête du type RetourneUnPanierParIdQuery avec un id '(.*)'")]
        public async Task QuandOnEnvoieUneRequeteDuTypeRetourneUnPanierParIdQueryAvecUnId(int id)
        {
            try
            {
                _byIdQuery = new RetourneUnPanierParIdQuery(id);
                _panierResult = await _byIdHandler.Handle(_byIdQuery, new System.Threading.CancellationToken());
            }
            catch (Exception e)
            {
                _exceptionThrown = e;
            }
        }
        
        [Then(@"on obtient '(.*)' exception du type '(.*)'")]
        public void AlorsOnObtientExceptionDuType(int nbException, string exceptionType)
        {
            if (nbException==0)
            {
                Assert.IsNull(_exceptionThrown, "Une exception est survenue");
            }   
            else
            {
                Assert.IsNotNull(_exceptionThrown, "Aucune exception n'a été levée");
                Assert.AreEqual(exceptionType, _exceptionThrown.GetType().Name, "Le type d'exception remonté n'est pas le bon");
            }
        }
        
        [Then(@"on obtient bien '(.*)' panier avec un id '(.*)' contenant '(.*)' articles")]
        public void AlorsOnObtientBienUnPanierAvecUnIdContenantArticles(int nbPaniers, int id, int? nbArticles)
        {
            if (nbPaniers == 1)
            {
                Assert.AreEqual(id, _panierResult.Id, "L'identifiant retourné n'est pas celui attendu");
                Assert.AreEqual(nbArticles, _panierResult.Articles.Count());
            }
            else if (nbPaniers == 0)
            {
                Assert.IsNull(_panierResult, "Un panier a été trouvé");
            }
            else
            {
                Assert.Fail("La fonction retourne plus d'un panier");
            }
        }

        [Given(@"un gestionnaire de requête du type Retourne TousLesPaniers")]
        public void SoitUnGestionnaireDeRequeteDuTypeRetourneTousLesPaniers()
        {
            _tousHandler = new RetourneTousLesPaniersQueryHandler(_repoPaniers, _mapper);
        }

        [When(@"on envoie une requête du type RetourneTousLesPaniersQuery")]
        public async Task QuandOnEnvoieUneRequeteDuTypeRetourneTousLesPaniersQuery()
        {
            try
            {
                _tousQuery = new RetourneTousLesPaniersQuery();
                _paniersResult = await _tousHandler.Handle(_tousQuery, new System.Threading.CancellationToken());
            }
            catch (Exception e)
            {
                _exceptionThrown = e;
            }
        }

        [Then(@"on obtient bien '(.*)' panier")]
        public void AlorsOnObtientBienPanier(int nbPaniers)
        {
            Assert.AreEqual(nbPaniers, _paniersResult.Count, "Le nombre de panier n'est pas celui attendu");
        }

    }
}
