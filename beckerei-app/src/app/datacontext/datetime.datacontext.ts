import { Injectable } from '@angular/core';
import { Moment } from 'moment';
import { ConfigService } from '../ConfigService';
import { DateDto } from '../dtos/DateDto';

@Injectable()
export class DatetimeDatacontext
{
    constructor(private configService: ConfigService) {

    }
  
    public GetAll() {
        return this.configService.get<DateDto>(`api/Datetime/all`);
    }

    public GetDatetimeLocal() {
        return this.configService.get<Date>(`api/Datetime/`);
    }

    public GetDatetimeUtc() {
        return this.configService.get<Date>(`api/Datetime/utc`);
    }

    public GetDateTimeOffsetLocal() {
        return this.configService.get<Date>(`api/Datetime/offsetlocal`);
    }

    public GetDateTimeOffsetUtc() {
        return this.configService.get<Date>(`api/Datetime/offsetutc`);
    }

    public PostDateTime(date: Date) {
        return this.configService.post<Date>(`api/Datetime/`,date);
    }

    public PostMoment(date: Moment) {
        return this.configService.post<Moment>(`api/Datetime/offset`,date);
    }
}