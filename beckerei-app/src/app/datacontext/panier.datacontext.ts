import { Injectable } from '@angular/core';
import { ConfigService } from '../ConfigService';
import { PanierDto } from '../dtos/PanierDto';

@Injectable()
export class PanierDatacontext
{
    constructor(private configService: ConfigService) {

    }
  
    public GetAll() {
        return this.configService.get(`api/Panier/`);
    }

    public GetById(id : number) {
        return this.configService.get<PanierDto>(`api/Panier/${id}`);
    }

}