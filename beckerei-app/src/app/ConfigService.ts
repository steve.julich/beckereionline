import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const optionRequete = {
  headers: new HttpHeaders({ 
    'Access-Control-Allow-Origin':'*'
  })
};

@Injectable()
export class ConfigService {

  private baseUrl:string = "https://localhost:44324/";


  
  constructor(public http: HttpClient) {
  }

  get<T>(url:string){
    return this.http.get<T>(`${this.baseUrl}${url}`, optionRequete);
  }

  post<T>(url:string, body: any){
    return this.http.post<T>(`${this.baseUrl}${url}`, body, optionRequete);
  }

  getBaseURl(){
    return this.baseUrl;
  }

  getHubURL(){
    return this.getBaseURl()+"/statehub";
  }
}
