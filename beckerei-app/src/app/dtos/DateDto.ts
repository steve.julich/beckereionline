export class DateDto{
    dateTime : Date;
    dateTimeLocal : Date;
    dateTimeUtc : Date;
    dateTimeOffsetLocal : Date;
    dateTimeOffsetUtc : Date;
}