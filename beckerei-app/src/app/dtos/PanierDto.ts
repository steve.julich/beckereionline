import { ArticleDansLePanierDto } from "./ArticleDansLePanierDto";
import { ClientDto } from "./ClientDto";

export class PanierDto{
    Id : number
    dateCreation : Date;
    Client : ClientDto;
    Articles : ArticleDansLePanierDto[]
}