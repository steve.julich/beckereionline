import { ArticleForListDto } from "./ArticleForListDto";

export class ArticleDansLePanierDto{
    Article : ArticleForListDto;
    Quantite : number;
}