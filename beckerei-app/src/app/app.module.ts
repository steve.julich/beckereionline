import { HttpClient, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { ConfigService } from './ConfigService';
import { DatetimeDatacontext } from './datacontext/datetime.datacontext';
import { PanierDatacontext } from './datacontext/panier.datacontext';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NgbModule
  ],
  providers: [
    ConfigService,
    DatetimeDatacontext,
    PanierDatacontext
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
