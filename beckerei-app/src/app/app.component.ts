import { Component } from '@angular/core';
import { DatetimeDatacontext } from './datacontext/datetime.datacontext';
import { PanierDatacontext } from './datacontext/panier.datacontext';
import { DateDto } from './dtos/DateDto';
import { PanierDto } from './dtos/PanierDto';
import * as moment from 'moment';
import { Moment } from 'moment';
import { NgbDate, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'beckerei-app';

  selectedDate : NgbDateStruct;
  selectedMoment : NgbDateStruct;

  datetime : Date;
  datetimeEF : Date;
  datetimeLocal : Date;
  datetimeUtc : Date;
  datetimeOffsetLocal : Date;
  datetimeOffsetUtc : Date;

  constructor(private datetimeDatacontext : DatetimeDatacontext,
            private panierDatacontext : PanierDatacontext) {
  }

  ngOnInit() {
    this.datetimeDatacontext.GetAll()
    .subscribe(( dateDto : DateDto ) => {
      this.datetime = dateDto.dateTime;
      this.datetimeLocal = dateDto.dateTimeLocal;
      this.datetimeUtc = dateDto.dateTimeUtc;
      this.datetimeOffsetLocal = dateDto.dateTimeOffsetLocal;
      this.datetimeOffsetUtc = dateDto.dateTimeOffsetUtc;
    }, error=>{
      console.log("Erreur");
    });
    this.panierDatacontext.GetById(2)
    .subscribe(( panier : PanierDto ) => {
      this.datetimeEF = panier.dateCreation;
    }, error=>{
      console.log("Erreur");
    });
    
  }

  postDate(event : NgbDate){
    //var date = new Date(event.year, event.month, event.day);
    var date = new Date(Date.UTC(event.year, event.month-1, event.day));
    //var date = new Date("2021-10-03 23:59:59.0");
    this.datetimeDatacontext.PostDateTime(date).subscribe(( ) => {
      console.log("success");
    }, error=>{
      console.log("Erreur");
    });
  }

  postMoment(event : NgbDate){
    var mom = moment().date(event.day).month(event.month-1).year(event.year).startOf('day');//.utc(true);
    //var mom = moment("2021-10-03 23:59:59.0", "YYYY-MMM-DD hh:mm:ss.c")
    this.datetimeDatacontext.PostMoment(mom).subscribe(( ) => {
      console.log("success");
    }, error=>{
      console.log("Erreur");
    });
  }

}
