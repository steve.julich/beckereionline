﻿using Beckerei.DomainModel.Clients.Entities;
using Beckerei.DomainModel.Clients.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Beckerei.Infrastructure.Core.Repositories
{
    public class RepositoryClients : IRepositoryClients
    {
        private readonly BekereiContext _dbContext;

        public RepositoryClients(BekereiContext dbContext)
        {
            _dbContext = dbContext;
        }

        public Task<Client> GetClientByIdAsync(int id)
        {
            if (id <= 0)
                throw new Exception("Veuillez saisir un identifiant valide");

            return _dbContext.Set<Client>().SingleOrDefaultAsync(c=>c.Id == id);
        }

        public Task<List<Client>> GetClientsAsync(int? top = null, int? skip = null)
        {
            if ((top == null && skip != null) ||
                (top != null && skip == null))
                throw new Exception("Les paramètres top et skip doivent être utilisé ensemble");

            var clients = _dbContext.Set<Client>().AsQueryable();

            if (top != null)
                clients = clients.Take(top.Value);
            if (skip != null)
                clients = clients.Skip(skip.Value);

            return clients.ToListAsync();
        }

        public Task<Client> SaveOrUpdateAsync(Client client)
        {
            return Task.Run(() =>
            {
                if (client.Id == 0)
                {
                    _dbContext.Set<Client>().Add(client);
                }
                _dbContext.SaveChanges();
                return client;
            });
        }
    }
}
