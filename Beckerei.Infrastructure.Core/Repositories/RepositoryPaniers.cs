﻿using Beckerei.DomainModel.Articles.Entities;
using Beckerei.DomainModel.Paniers.Entities;
using Beckerei.DomainModel.Paniers.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Beckerei.Infrastructure.Core.Repositories
{
    public class RepositoryPaniers : IRepositoryPaniers
    {
        private readonly BekereiContext _dbContext;

        public RepositoryPaniers(BekereiContext dbContext)
        {
            _dbContext = dbContext;
        }

        public Task<List<Panier>> GetPanierAsync(int? top = null, int? skip = null)
        {
            if ((top == null && skip != null) ||
                (top != null && skip == null))
                throw new Exception("Les paramètres top et skip doivent être utilisé ensemble");

            var paniers = _dbContext.Set<Panier>().Include(x=>x.Client).Include(x=>x.Articles).AsQueryable();
           
            if (top != null)
                paniers = paniers.Take(top.Value);
            if (skip != null)
                paniers = paniers.Skip(skip.Value);

            return paniers.ToListAsync();
        }

        public Task<Panier> GetPanierByIdAsync(int id)
        {
            if (id <= 0)
                throw new Exception("Veuillez saisir un identifiant valide");

            return _dbContext.Set<Panier>()
                .Include(p=>p.Articles)
                .ThenInclude(a=>a.Article)
                .SingleOrDefaultAsync(p => p.Id == id);
        }

        public Task<Panier> SaveOrUpdatePanierAsync(Panier panier)
        {
            return Task.Run(() =>
            {
                if (panier.Id == 0)
                {
                    _dbContext.Set<Panier>().Add(panier);
                }
                _dbContext.SaveChanges();
                return panier;
            });
        }

        public Task SupprimePanierAsync(int panierId)
        {
            _dbContext.Set<Panier>().Remove(GetPanierByIdAsync(panierId).Result);
            return _dbContext.SaveChangesAsync();
        }
    }
}
