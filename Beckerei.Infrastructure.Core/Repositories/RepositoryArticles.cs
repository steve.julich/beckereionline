﻿using Beckerei.DomainModel.Articles.Entities;
using Beckerei.DomainModel.Articles.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Beckerei.Infrastructure.Core.Repositories
{
    public class RepositoryArticles : IRepositoryArticles
    {
        private readonly BekereiContext _dbContext;

        public RepositoryArticles(BekereiContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Article> AjoutArticleAsync(Article article)
        {
            if (article.Id > 0)
                throw new Exception("Impossible d'ajouter un article ayant déjà un id défini");

            _dbContext.Set<Article>().Add(article);
            await _dbContext.SaveChangesAsync();
            return article;
        }

        public async Task<Article> GetArticleByIdAsync(int id)
        {
            if (id <= 0)
                throw new Exception("Veuillez saisir un identifiant valide");

            return await _dbContext.Set<Article>().SingleOrDefaultAsync(a => a.Id == id);
        }

        public async Task<List<Article>> GetArticlesAsync(bool enStockUniquement = true, int? top = null, int? skip = null)
        {
            if ((top == null && skip != null) ||
                (top != null && skip == null))
                throw new Exception("Les paramètres top et skip doivent être utilisé ensemble");

            var articles = _dbContext.Set<Article>().AsQueryable();
            
            if (enStockUniquement)
                articles = articles.Where(a => a.QuantiteEnStock > 0);

            if (top != null)
                articles = articles.Take(top.Value);
            if (skip != null)
                articles = articles.Skip(skip.Value);

            return await articles.ToListAsync();
        }

        public async Task<List<Article>> GetArticlesByNameAsync(string pattern)
        {
            return await _dbContext.Set<Article>().Where(a => a.Nom.Contains(pattern)).ToListAsync();
        }
    }
}
