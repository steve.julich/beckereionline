﻿using Beckerei.DomainModel.Clients.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Beckerei.Infrastructure.Core.Mappings
{
    public class ClientMap : IEntityTypeConfiguration<Client>
    {
        public void Configure(EntityTypeBuilder<Client> builder)
        {
            builder.ToTable("Client");
            builder.Property(p => p.Id).UseIdentityColumn();
            builder.Property(p => p.Nom);
            builder.Property(p => p.Prenom);
            builder.Property(p => p.Adresse1);
            builder.Property(p => p.Adresse2);
            builder.Property(p => p.CodePostal);
            builder.Property(p => p.Ville);
            builder.HasMany(p => p.Paniers).WithOne(a => a.Client);

        }
    }
}