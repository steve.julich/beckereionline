﻿using Beckerei.DomainModel.Paniers.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Beckerei.Infrastructure.Core.Mappings
{
    public class PanierMap : IEntityTypeConfiguration<Panier>
    {
        public void Configure(EntityTypeBuilder<Panier> builder)
        {
            builder.ToTable("Panier");
            builder.Property(p => p.Id).UseIdentityColumn();
            builder.Property(p => p.DateCreation);
            builder.HasOne(p => p.Client).WithMany(c => c.Paniers);
            builder.HasMany(p => p.Articles).WithOne(a=>a.Panier);
            
        }
    }
}
