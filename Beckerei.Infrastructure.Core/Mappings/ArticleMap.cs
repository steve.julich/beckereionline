﻿using Beckerei.DomainModel.Articles.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Beckerei.Infrastructure.Core.Mappings
{
    public class ArticleMap : IEntityTypeConfiguration<Article>
    {
        public void Configure(EntityTypeBuilder<Article> builder)
        {
            builder.ToTable("Article");
            builder.Property(a => a.Id).UseIdentityColumn();
            builder.Property(a => a.Nom);
            builder.Property(a => a.Poids);
            builder.Property(a => a.PrixUnitaire);
            builder.Property(a => a.QuantiteEnStock);
        }
    }
}
