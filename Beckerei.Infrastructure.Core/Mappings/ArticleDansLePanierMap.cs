﻿using Beckerei.DomainModel.Paniers.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Beckerei.Infrastructure.Core.Mappings
{
    public class ArticleDansLePanierMap : IEntityTypeConfiguration<ArticleDansLePanier>
    {
        public void Configure(EntityTypeBuilder<ArticleDansLePanier> builder)
        {
            builder.ToTable("ArticleDansLePanier");
            builder.HasKey(a => new { a.ArticleId, a.PanierId });
            builder.HasOne(p=>p.Panier).WithMany(p=>p.Articles).IsRequired().HasForeignKey(a=>a.PanierId);
            builder.HasOne(p => p.Article).WithMany().IsRequired().HasForeignKey(a => a.ArticleId);
            builder.Property(p => p.Quantite);
            

        }
    }
}
