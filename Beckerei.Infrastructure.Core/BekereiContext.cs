﻿using Beckerei.Infrastructure.Core.Mappings;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Beckerei.Infrastructure.Core
{
    public class BekereiContext : DbContext
    {
        public BekereiContext()
        {
        }

        public BekereiContext(DbContextOptions options) : base (options)
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Data Source=localhost; Initial Catalog=beckereiDB;Integrated Security=True;",
                                           sqlOptions => sqlOptions.MigrationsAssembly(typeof(BekereiContext).GetTypeInfo().
                                                                                                Assembly.GetName().Name));
            base.OnConfiguring(optionsBuilder);
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(ArticleMap).Assembly);
            base.OnModelCreating(modelBuilder);
        }
    }
}
