﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Beckerei.Dtos.Datetime
{
    public class DateTimeDto
    {
        public DateTime DateTime { get; set; }
        public DateTime DateTimeLocal { get; set; }

        public DateTime DateTimeUtc { get; set; }

        public DateTimeOffset DateTimeOffsetLocal { get; set; }

        public DateTimeOffset DateTimeOffsetUtc { get; set; }
    }
}
