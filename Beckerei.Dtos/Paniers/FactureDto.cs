﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Beckerei.Dtos.Paniers
{
    public class FactureDto
    {
        public decimal Prixtotal { get; set; }
        public decimal PoidsTotal { get; set; }
        public int NombreArticleTotal { get; set; }

        public List<LigneArticlePourFactureDto> Articles { get; set; }
    }
}
