﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Beckerei.Dtos.Paniers
{
    public class LigneArticlePourFactureDto
    {
        public string Nom { get; set; }

        public int Quantite { get; set; }

        public decimal PrixUnitaire { get; set; }

        public decimal PrixTotal { get; set; }

        public decimal PoidsTotal { get; set; }
    }
}
