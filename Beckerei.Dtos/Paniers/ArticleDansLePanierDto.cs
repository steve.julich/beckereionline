﻿using Beckerei.Dtos.Articles;
using System;
using System.Collections.Generic;
using System.Text;

namespace Beckerei.Dtos.Paniers
{
    public class ArticleDansLePanierDto
    {
        public ArticleForListDto Article { get; set; }

        public int Quantite { get; set; }
    }
}
