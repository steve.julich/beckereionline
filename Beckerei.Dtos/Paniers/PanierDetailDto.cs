﻿using Beckerei.Dtos.Articles;
using Beckerei.Dtos.Clients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Beckerei.Dtos.Paniers
{
    public class PanierDetailDto
    {
        public int Id { get; set; }

        public DateTime DateCreation { get; set; }

        public ClientDto Client { get; set; }

        public List<ArticleDansLePanierDto> Articles { get; set; }
    }
}
