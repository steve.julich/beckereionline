﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Beckerei.Dtos.Paniers
{
    public class PanierForListDto
    {
        public int Id { get; set; }

        public string Client { get; set; }

        public int NbArticles { get; set; }
    }
}
