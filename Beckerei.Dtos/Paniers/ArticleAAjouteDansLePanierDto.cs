﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Beckerei.Dtos.Paniers
{
    public class ArticleAAjouteDansLePanierDto
    {
        public int ArticleId { get; set; }

        public int Quantite { get; set; }
    }
}
