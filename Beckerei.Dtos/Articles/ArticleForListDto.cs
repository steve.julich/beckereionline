﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Beckerei.Dtos.Articles
{
    public class ArticleForListDto
    {
        public int Id { get; set; }

        public string Nom { get; set; }

        public decimal PrixUnitaire { get; set; }
    }
}
