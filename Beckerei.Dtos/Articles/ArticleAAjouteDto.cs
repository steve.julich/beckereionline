﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Beckerei.Dtos.Articles
{
    public class ArticleAAjouteDto
    {

        public string Nom { get; set; }

        public decimal Poids { get; set; }

        public decimal PrixUnitaire { get; set; }

        public int QuantiteEnStock { get; set; }
    }
}
