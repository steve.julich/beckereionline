﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Beckerei.Dtos.Clients
{
    public class ClientAAjouteDto
    {
        public string Nom { get; set; }

        public string Prenom { get; set; }

        public string Adresse1 { get; set; }

        public string Adresse2 { get; set; }

        public string Ville { get; set; }

        public string CodePostal { get; set; }
    }
}
