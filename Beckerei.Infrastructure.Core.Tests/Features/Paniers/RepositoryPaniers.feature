﻿Fonctionnalité: RepositoryPaniers

Scénario: Appel de la fonction GetPanierAsync sans paramètres
Etant donné Une base de données de test à jour
Et un repository de panier
Quand on appelle la fonction GetPanierAsync
Alors on obtient aucune exception
Et on obtient 4 résultats