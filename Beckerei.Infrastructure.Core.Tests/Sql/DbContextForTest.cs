﻿using AutoFixture;
using Beckerei.DomainModel.Paniers.Entities;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Beckerei.Infrastructure.Core.Tests.Sql
{
    public class DbContextForTest
    {
        const string testConnectionString = "Server=localhost,1434; Initial Catalog=beckereiDB; User ID=sa; Password=CorrectHorseBatteryStapleFor$";

        private BekereiContext bekereiContext;
        private static DbContextForTest _instance { get; set; }
        private DbContextForTest()
        {
            //var options = new DbContextOptionsBuilder<BekereiContext>()
            // .UseInMemoryDatabase(databaseName: "test");
            var options = new DbContextOptionsBuilder<BekereiContext>()
             .UseSqlServer(testConnectionString,
                                           sqlOptions => sqlOptions.MigrationsAssembly(typeof(BekereiContext).GetTypeInfo().
                                                                                                Assembly.GetName().Name))
             .Options;

            Task.WaitAll(WaitUntilDatabaseAvailableAsync());

            bekereiContext = new BekereiContext(options);

            //Migration
            bekereiContext.Database.Migrate();
            //Simutation de seed
            Fixture fixture = new Fixture();
            bekereiContext.Set<Panier>().AddRange(fixture.Build<Panier>().Without(p=>p.Id).Without(p => p.Client).Without(p => p.Articles).CreateMany(4));
            bekereiContext.SaveChanges();
        }

        public static BekereiContext GetContext()
        {
            if (_instance == null)
                _instance = new DbContextForTest();

            return _instance.bekereiContext;
        }

        private async Task WaitUntilDatabaseAvailableAsync()
        {
            var start = DateTime.UtcNow;
            const int maxWaitTimeSeconds = 180;
            await Task.Delay(30000);
            var connectionEstablished = false;
            var sqlConnectionString = testConnectionString;
            using (var sqlConnection = new SqlConnection(sqlConnectionString))
            {
                while (!connectionEstablished && start.AddSeconds(maxWaitTimeSeconds) > DateTime.UtcNow)
                {
                    try
                    {
                        await sqlConnection.OpenAsync();
                        connectionEstablished = true;
                    }
                    catch(Exception e)
                    {
                        // If opening the SQL connection fails, SQL Server is not ready yet
                        await Task.Delay(10000);
                    }
                }
            }
            if (!connectionEstablished)
            {
                throw new Exception($"Connection to the SQL docker database could not be established within {maxWaitTimeSeconds} seconds.");
            }

            return;
        }
    }
}
