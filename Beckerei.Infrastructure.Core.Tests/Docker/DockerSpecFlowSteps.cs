﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace Beckerei.Infrastructure.Core.Tests.Docker
{
    [Binding]
    public class DockerSpecFlowSteps
    {
        private DockerService _service;

        [Before]
        public void StartDocker()
        {
            _service = new DockerService();
        }

        [Given(@"Une base de données de test à jour")]
        public async Task SoitUnServiceDeMappingEntiteVersDto()
        {
            _service.Initialize();
            await _service.StartSqlServerImage();
        }

        [After]
        public async Task StopDocker()
        {
            await _service.DisposeAsync();
        }
    }
}
