﻿using Docker.DotNet;
using Docker.DotNet.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Beckerei.Infrastructure.Core.Tests.Docker
{
    public class DockerService : IAsyncDisposable
    {
        const string SQLSERVER_IMAGENAME = "beckereidb-sql-server";
        const string SQLSERVER_CONTAINERNAME = "sqlserver-for-test";

        private DockerClient client;
        
        public DockerService()
        {

        }

        public void Initialize()
        {
            if (client == null)
            {
                client = new DockerClientConfiguration()
                        .CreateClient();
            }
        }

        public async Task StartSqlServerImage()
        {
            var response = await client.Containers.CreateContainerAsync(new CreateContainerParameters()
            {
                Image = SQLSERVER_IMAGENAME,
                Name = SQLSERVER_CONTAINERNAME,
                Env = new List<string>() { "ACCEPT_EULA=Y", "SA_PASSWORD=CorrectHorseBatteryStapleFor$" },
                HostConfig = new HostConfig
                {
                    PortBindings = new Dictionary<string, IList<PortBinding>>
                    {
                        {
                            "1433/tcp",
                            new List<PortBinding>
                            {
                                new PortBinding
                                {
                                    HostPort = "1434/tcp"
                                }
                            }
                        }
                    }
                },
                ExposedPorts = new Dictionary<string, EmptyStruct>()
                {
                    { "1434/tcp", new EmptyStruct(){}}
                }
            });
            await client.Containers.StartContainerAsync(
                response.ID,
                new ContainerStartParameters()
                );
        }

        public async Task StopSqlServerImage()
        {
            await client.Containers.StopContainerAsync(SQLSERVER_CONTAINERNAME, new ContainerStopParameters());
            await client.Containers.RemoveContainerAsync(SQLSERVER_CONTAINERNAME,
                new ContainerRemoveParameters { Force = true });
        }

        public async ValueTask DisposeAsync()
        {
            if (client != null)
            {
                await StopSqlServerImage();
                client.Dispose();
            }
        }

    }
}
