﻿using Beckerei.DomainModel.Paniers.Entities;
using Beckerei.DomainModel.Paniers.Repositories;
using Beckerei.Infrastructure.Core.Repositories;
using Beckerei.Infrastructure.Core.Tests.Sql;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace Beckerei.Infrastructure.Core.Tests.Steps.Paniers
{
    [Binding]
    public class RepositoryPaniersStep
    {
        private IRepositoryPaniers _repositoryPaniers;
        private Exception _exceptionThrown;
        private List<Panier> paniersResult;

        [AfterScenario]
        public void Afterscenario()
        {
            _repositoryPaniers = null;
            _exceptionThrown = null;
        }

        [Given(@"un repository de panier")]
        public void SoitUnRepositoryDePanier()
        {
            _repositoryPaniers = new RepositoryPaniers(DbContextForTest.GetContext());
        }
        
        [When(@"on appelle la fonction GetPanierAsync")]
        public async Task QuandOnAppelleLaFonctionGetPanierAsync()
        {
            try
            {
                paniersResult = await _repositoryPaniers.GetPanierAsync();
            }
            catch(Exception e)
            {
                _exceptionThrown = e;
            }
        }

        [Then(@"on obtient aucune exception")]
        public void AlorsOnObtientAucuneException()
        {
            Assert.IsNull(_exceptionThrown, "Une exception a été levée");
        }


        [Then(@"on obtient (.*) résultats")]
        public void AlorsOnObtientResultats(int nbPaniersAttendus)
        {
            if (nbPaniersAttendus > 0)
            {
                Assert.IsNotNull(paniersResult, "Aucun panier n'a été retourné");
                Assert.AreEqual(nbPaniersAttendus, paniersResult.Count, "Le nombre de paniers retourné ne coincide pas");
            }
        }
    }
}
