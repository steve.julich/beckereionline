﻿using Beckerei.Dtos.Articles;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Beckerei.DomainModel.Articles.Commands
{
    public class CreeNouvelArticleCommand : IRequest<ArticleDetailDto>
    {
        public string Nom { get; set; }

        public decimal Poids { get; set; }

        public decimal PrixUnitaire { get; set; }

        public int QuantiteEnStock { get; set; }
    }
}
