﻿using AutoMapper;
using Beckerei.DomainModel.Articles.Commands;
using Beckerei.DomainModel.Articles.Entities;
using Beckerei.DomainModel.Articles.Repositories;
using Beckerei.Dtos.Articles;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Beckerei.DomainModel.Articles.CommandHandlers
{
    public class CreeNouvelArticleCommandHandler : IRequestHandler<CreeNouvelArticleCommand, ArticleDetailDto>
    {
        private readonly IRepositoryArticles _articlesRepository;
        private readonly IMapper _mapper;

        public CreeNouvelArticleCommandHandler(IRepositoryArticles articlesRepository, IMapper mapper)
        {
            _articlesRepository = articlesRepository;
            _mapper = mapper;
        }

        public async Task<ArticleDetailDto> Handle(CreeNouvelArticleCommand request, CancellationToken cancellationToken)
        {
            var article = new Article()
            {
                Nom = request.Nom,
                Poids = request.Poids,
                PrixUnitaire = request.PrixUnitaire,
                QuantiteEnStock = request.QuantiteEnStock
            };
            article = await _articlesRepository.AjoutArticleAsync(article);

            return _mapper.Map<ArticleDetailDto>(article);
        }
    }
}
