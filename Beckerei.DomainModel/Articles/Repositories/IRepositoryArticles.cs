﻿using Beckerei.DomainModel.Articles.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Beckerei.DomainModel.Articles.Repositories
{
    public interface IRepositoryArticles
    {
        Task<Article> AjoutArticleAsync(Article article);

        Task<Article> GetArticleByIdAsync(int id);

        Task<List<Article>> GetArticlesByNameAsync(string pattern);

        Task<List<Article>> GetArticlesAsync(bool enStockUniquement = true, int? top=null, int? skip=null);

    }
}
