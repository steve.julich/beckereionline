﻿using Beckerei.DomainModel.Articles.Commands;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Beckerei.DomainModel.Articles.Specifications
{
    public class CreeNouvelArticleCommandSpecification : AbstractValidator<CreeNouvelArticleCommand>
    {
        public CreeNouvelArticleCommandSpecification()
        {
            RuleFor(x => x.Nom).Must(x => x.ToLower() != "chocolatine").WithMessage("On dit 'petit pain au chocolat' ou 'Chockolatweckele' !!");
            RuleFor(x => x.PrixUnitaire).GreaterThanOrEqualTo(0).WithMessage("Le prix doit être positif");
        }
    }

}
