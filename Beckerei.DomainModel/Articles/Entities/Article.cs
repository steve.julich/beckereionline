﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Beckerei.DomainModel.Articles.Entities
{
    public class Article
    {
        public int Id { get; set; }

        public string Nom { get; set; }

        public decimal Poids { get; set; }

        public decimal PrixUnitaire { get; set; }

        public int QuantiteEnStock { get; set; }

    }
}
