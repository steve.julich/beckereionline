﻿using Beckerei.DomainModel.Articles.Repositories;
using Beckerei.DomainModel.Paniers.Commands;
using Beckerei.DomainModel.Paniers.Repositories;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Beckerei.DomainModel.Paniers.Specifications
{
    public class AjoutArticleAuPanierCommandSpecification : AbstractValidator<AjoutArticleAuPanierCommand>
    {
        private readonly IRepositoryPaniers _paniersRepository;
        private readonly IRepositoryArticles _articlesRepository;

        public int QuantiteRestanteEnStock { get; set; }

        public AjoutArticleAuPanierCommandSpecification(IRepositoryPaniers paniersRepository, IRepositoryArticles articlesRepository)
        {
            _paniersRepository = paniersRepository;
            _articlesRepository = articlesRepository;

            RuleFor(x => x.PanierId).Must(x => PanierExists(x)).WithMessage(x => $"Impossible de trouver le panier {x.PanierId}");
            RuleFor(x => x.ArticleId).Must(x => ArticleExists(x)).WithMessage(x => $"Impossible de trouver l'article {x.ArticleId}");
            RuleFor(x => x).Must(x => StockSuffisant(x.ArticleId, x.Quantite)).WithMessage(x => $"Le stock est insuffisant pour répondre à la demande ! (reste {QuantiteRestanteEnStock} unités)");
        }

        private bool PanierExists(int panierId)
        {
            var panier = _paniersRepository.GetPanierByIdAsync(panierId).Result;
            return (panier != null);
        }

        private bool ArticleExists(int articleId)
        {
            var article = _articlesRepository.GetArticleByIdAsync(articleId).Result;
            return (article != null);
        }

        private bool StockSuffisant(int articleId, int quantite)
        {
            var article = _articlesRepository.GetArticleByIdAsync(articleId).Result;
            QuantiteRestanteEnStock = article.QuantiteEnStock;
            return (QuantiteRestanteEnStock >= quantite);
        }
    }
}
