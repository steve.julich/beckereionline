﻿using Beckerei.DomainModel.Clients.Repositories;
using Beckerei.DomainModel.Paniers.Commands;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Beckerei.DomainModel.Paniers.Specifications
{
    public class CreeNouveauPanierCommandSpecification : AbstractValidator<CreeNouveauPanierCommand>
    {
        private readonly IRepositoryClients _clientsRepository;

        public CreeNouveauPanierCommandSpecification(IRepositoryClients clientsRepository)
        {
            _clientsRepository = clientsRepository;

            RuleFor(x => x.ClientId).Must(x => ClientExists(x)).WithMessage("Impossible de créer un panier pour un client inexistant.");
        }

        private bool ClientExists(int clientId)
        {
            return (_clientsRepository.GetClientByIdAsync(clientId).Result != null);
        }
    }
}
