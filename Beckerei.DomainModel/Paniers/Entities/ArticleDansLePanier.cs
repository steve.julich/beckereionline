﻿using Beckerei.DomainModel.Articles.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Beckerei.DomainModel.Paniers.Entities
{
    public class ArticleDansLePanier
    {
        public Article Article { get; set; }

        public int ArticleId { get; set; }

        public Panier Panier { get; set; }

        public int PanierId { get; set; }

        public int Quantite { get; set; }
    }
}
