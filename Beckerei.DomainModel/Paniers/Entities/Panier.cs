﻿using Beckerei.DomainModel.Articles.Entities;
using Beckerei.DomainModel.Clients.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Beckerei.DomainModel.Paniers.Entities
{
    public class Panier
    {
        public int Id { get; set; }

        public Client Client { get; set; }

        public List<ArticleDansLePanier> Articles { get; set; }

        public DateTime DateCreation { get; set; }

        public Panier()
        {
            Articles = new List<ArticleDansLePanier>();
        }

        public void AddArticle(Article article, int Quantite = 1)
        {
            Articles.Add(new ArticleDansLePanier()
            {
                Article = article,
                Panier = this,
                Quantite = Quantite
            });
        }
    }
}
