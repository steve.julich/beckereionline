﻿using Beckerei.DomainModel.Articles.Entities;
using Beckerei.DomainModel.Paniers.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Beckerei.DomainModel.Paniers.Repositories
{
    public interface IRepositoryPaniers
    {
        Task<Panier> GetPanierByIdAsync(int id);

        Task<List<Panier>> GetPanierAsync(int? top = null, int? skip = null);

        Task<Panier> SaveOrUpdatePanierAsync(Panier panier);

        Task SupprimePanierAsync(int panierId);
    }
}
