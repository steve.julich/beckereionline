﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Beckerei.DomainModel.Paniers.Commands
{
    public class CreeNouveauPanierCommand : IRequest<int>
    {
        public int ClientId { get; set; }
    }
}
