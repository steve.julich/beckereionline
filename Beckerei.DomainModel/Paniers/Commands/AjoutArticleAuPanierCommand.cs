﻿using Beckerei.Dtos.Paniers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Beckerei.DomainModel.Paniers.Commands
{
    public class AjoutArticleAuPanierCommand : IRequest<PanierDetailDto>
    {
        public int PanierId { get; set; }

        public int ArticleId { get; set; }

        public int Quantite { get; set; }
    }
}
