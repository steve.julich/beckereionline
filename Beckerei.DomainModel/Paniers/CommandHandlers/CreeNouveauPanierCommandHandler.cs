﻿using Beckerei.DomainModel.Clients.Repositories;
using Beckerei.DomainModel.Paniers.Commands;
using Beckerei.DomainModel.Paniers.Entities;
using Beckerei.DomainModel.Paniers.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Beckerei.DomainModel.Paniers.CommandHandlers
{
    public class CreeNouveauPanierCommandHandler : IRequestHandler<CreeNouveauPanierCommand, int>
    {
        private readonly IRepositoryPaniers _paniersRepository;
        private readonly IRepositoryClients _clientsRepository;

        public CreeNouveauPanierCommandHandler(IRepositoryPaniers paniersRepository, IRepositoryClients clientsRepository)
        {
            _paniersRepository = paniersRepository;
            _clientsRepository = clientsRepository;
        }

        public async Task<int> Handle(CreeNouveauPanierCommand request, CancellationToken cancellationToken)
        {
            var client = await _clientsRepository.GetClientByIdAsync(request.ClientId);
            var panier = new Panier()
            {
                Client = client,
                DateCreation = DateTime.Now
            };
            panier = await _paniersRepository.SaveOrUpdatePanierAsync(panier);
            return panier.Id;
        }
    }
}
