﻿using AutoMapper;
using Beckerei.DomainModel.Articles.Entities;
using Beckerei.DomainModel.Articles.Repositories;
using Beckerei.DomainModel.Paniers.Commands;
using Beckerei.DomainModel.Paniers.Entities;
using Beckerei.DomainModel.Paniers.Repositories;
using Beckerei.Dtos.Paniers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Beckerei.DomainModel.Paniers.CommandHandlers
{
    public class AjoutArticleAuPanierCommandHandler : IRequestHandler<AjoutArticleAuPanierCommand, PanierDetailDto>
    {
        private readonly IRepositoryPaniers _paniersRepository;
        private readonly IRepositoryArticles _articlesRepository;
        private readonly IMapper _mapper;

        public AjoutArticleAuPanierCommandHandler(IRepositoryPaniers paniersRepository, IMapper mapper, IRepositoryArticles articlesRepository)
        {
            _paniersRepository = paniersRepository;
            _mapper = mapper;
            _articlesRepository = articlesRepository;
        }

        public async Task<PanierDetailDto> Handle(AjoutArticleAuPanierCommand request, CancellationToken cancellationToken)
        {
            var panier = await _paniersRepository.GetPanierByIdAsync(request.PanierId);
            var articleDejaDansPanier = panier.Articles.SingleOrDefault(a => a.Article.Id == request.ArticleId);
            Article article;

            if (articleDejaDansPanier != null)
            {
                articleDejaDansPanier.Quantite += request.Quantite;
                article = articleDejaDansPanier.Article;
            }
            else
            {
                article = await _articlesRepository.GetArticleByIdAsync(request.ArticleId);
                panier.AddArticle(article, request.Quantite);
            }
            article.QuantiteEnStock -= request.Quantite;
            panier = await _paniersRepository.SaveOrUpdatePanierAsync(panier);
            return _mapper.Map<PanierDetailDto>(panier);
        }
    }
}
