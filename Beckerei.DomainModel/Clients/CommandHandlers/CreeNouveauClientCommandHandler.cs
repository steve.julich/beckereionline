﻿using AutoMapper;
using Beckerei.DomainModel.Clients.Commands;
using Beckerei.DomainModel.Clients.Entities;
using Beckerei.DomainModel.Clients.Repositories;
using Beckerei.Dtos.Clients;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Beckerei.DomainModel.Clients.CommandHandlers
{
    public class CreeNouveauClientCommandHandler : IRequestHandler<CreeNouveauClientCommand, ClientDetailDto>
    {
        private readonly IMapper _mapper;
        private readonly IRepositoryClients _clientsRepository;

        public CreeNouveauClientCommandHandler(IMapper mapper, IRepositoryClients clientsRepository)
        {
            _mapper = mapper;
            _clientsRepository = clientsRepository;
        }

        public async Task<ClientDetailDto> Handle(CreeNouveauClientCommand request, CancellationToken cancellationToken)
        {
            var client = new Client()
            {
                Nom = request.Nom,
                Prenom = request.Prenom,
                Adresse1 = request.Adresse1,
                Adresse2 = request.Adresse2,
                CodePostal = request.CodePostal,
                Ville = request.Ville
            };
            client =  await _clientsRepository.SaveOrUpdateAsync(client);
            return _mapper.Map<ClientDetailDto>(client);
        }
    }
}
