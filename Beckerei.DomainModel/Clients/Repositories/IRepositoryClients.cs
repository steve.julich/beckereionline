﻿using Beckerei.DomainModel.Clients.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Beckerei.DomainModel.Clients.Repositories
{
    public interface IRepositoryClients
    {
        Task<Client> GetClientByIdAsync(int id);

        Task<List<Client>> GetClientsAsync(int? top = null, int? skip = null);

        Task<Client> SaveOrUpdateAsync(Client client);
    }
}
