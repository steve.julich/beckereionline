﻿using Beckerei.DomainModel.Clients.Commands;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Beckerei.DomainModel.Clients.Specifications
{
    public class CreeNouveauClientSpecification : AbstractValidator<CreeNouveauClientCommand>
    {
        public CreeNouveauClientSpecification()
        {
            RuleFor(c => c.Nom).NotEmpty().WithMessage("Le nom du client est obligatoire.");
            RuleFor(c => c.Prenom).NotEmpty().WithMessage("Le prénom du client est obligatoire");
            RuleFor(c => c.Ville).Equal("BETSCHDORF").WithMessage("La ville n'est pas reconnue");
        }
    }
}
