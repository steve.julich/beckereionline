﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            calcul(null);
            calcul(EnumTest.TATA);
            calcul(EnumTest.TITI);
            calcul(EnumTest.TOTO);
            Console.ReadKey();
        }


        public static void calcul(EnumTest? test)
        {
            switch(test)
            {
                case EnumTest.TATA:
                    Console.WriteLine("TATA");
                    break;
                case EnumTest.TITI:
                    Console.WriteLine("TITI");
                    break;
                case EnumTest.TOTO:
                    Console.WriteLine("TOTO");
                    break;
            }
        }
    }
}
