using Beckerei.DomainModel;
using Beckerei.DomainModel.Articles.Entities;
using Beckerei.DomainModel.Articles.Repositories;
using Beckerei.DomainModel.Clients.Repositories;
using Beckerei.DomainModel.Paniers.Repositories;
using Beckerei.Infrastructure.Core;
using Beckerei.Infrastructure.Core.Repositories;
using Beckerei.ReadDomainModel.Mappings;
using Beckerei.ReadDomainModel.Paniers.Queries;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace BeckereiOnline
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddCors(options =>
            {
                options.AddPolicy(name: "localhost",
                                  builder =>
                                  {
                                      builder.AllowAnyMethod().AllowAnyHeader().AllowAnyOrigin();
                                  });
            });
            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen();
            services.AddEntityFrameworkSqlServer()
                  .AddDbContext<BekereiContext>(options =>
                  {
                      options.UseSqlServer(Configuration.GetConnectionString("beckereiDatabase"),
                                           sqlOptions => sqlOptions.MigrationsAssembly(typeof(BekereiContext).GetTypeInfo().
                                                                                                Assembly.GetName().Name));
                  },
                  ServiceLifetime.Scoped // Note that Scoped is the default choice
                                         // in AddDbContext. It is shown here only for
                                         // pedagogic purposes.
                  );
            services.AddAutoMapper(typeof(MapperProfile).GetTypeInfo().Assembly);
            //Repositories
            services.AddScoped<IRepositoryPaniers, RepositoryPaniers>();
            services.AddScoped<IRepositoryClients, RepositoryClients>();
            services.AddScoped<IRepositoryArticles, RepositoryArticles>();

            //MediatR
            services.AddMediatR(new Type[] { typeof(RetourneTousLesPaniersQuery), typeof(Article) });
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(ValidationBehavior<,>));
            services.AddValidatorsFromAssemblyContaining(typeof(ValidationBehavior<,>));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, BekereiContext dbContext)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseCors("localhost");
            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "BeckereiOnline V1");
                c.RoutePrefix = string.Empty;
            });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            dbContext.Database.Migrate();
        }
    }
}
