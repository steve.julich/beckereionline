﻿using Beckerei.Dtos.Datetime;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeckereiOnline.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DatetimeController : ControllerBase
    {
        private ILogger _logger { get; set; }
        public DatetimeController(ILogger<DatetimeController> logger)
        {
            _logger = logger;
        }

        // GET api/<DatetimeController>/all
        [HttpGet("all")]
        public IActionResult GetAll()
        {
            _logger.LogInformation("test");
            var offset = TimeZoneInfo.Local.GetUtcOffset(DateTime.UtcNow);
            var datetime = new DateTime(2021, 10, 03, 23, 59, 59);
            var datetimeLocal = new DateTime(2021, 10, 03, 23, 59, 59, DateTimeKind.Local);
            var datetimeUtc = new DateTime(2021, 10, 03, 23, 59, 59, DateTimeKind.Utc);
            //var datetimeOffsetLocal = new DateTimeOffset(2021, 10, 03, 23, 59, 59, offset);
            var datetimeOffsetLocal = new DateTimeOffset(datetime, offset);
            var datetimeOffsetUtc = new DateTimeOffset(2021, 10, 03, 23, 59, 59, TimeSpan.FromHours(0));

            return Ok(new DateTimeDto()
            {
                DateTime = datetime,
                DateTimeLocal = datetimeLocal,
                DateTimeUtc = datetimeUtc,
                DateTimeOffsetLocal = datetimeOffsetLocal,
                DateTimeOffsetUtc = datetimeOffsetUtc
            });
        }
        // GET api/<DatetimeController>/
        [HttpGet]
        public IActionResult Get()
        {
            var datetime = DateTime.Now;
            return Ok(datetime);
        }

        // GET api/<DatetimeController>/utc
        [HttpGet("utc")]
        public IActionResult GetUtc()
        {
            var datetime = DateTime.UtcNow;
            return Ok(datetime);
        }

        // GET api/<DatetimeController>/offset
        [HttpGet("offset")]
        public IActionResult GetOffset()
        {
            var datetime = DateTimeOffset.Now;
            return Ok(datetime);
        }

        // GET api/<DatetimeController>/offsetutc
        [HttpGet("offsetutc")]
        public IActionResult GetOffsetUtc()
        {
            var datetime = DateTimeOffset.UtcNow;
            return Ok(datetime);
        }

        [HttpPost]
        public IActionResult Post([FromBody] DateTime dateTime)
        {
            Console.WriteLine(dateTime);
            return Ok(dateTime);
        }

        [HttpPost("offset")]
        public IActionResult PostOffset([FromBody] DateTimeOffset dateTime)
        {
            Console.WriteLine(dateTime);
            return Ok(dateTime);
        }
    }
}
