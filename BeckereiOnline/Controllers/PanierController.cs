﻿using Beckerei.DomainModel.Paniers.Commands;
using Beckerei.Dtos.Paniers;
using Beckerei.ReadDomainModel.Paniers.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BeckereiOnline.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PanierController : ControllerBase
    {
        private readonly IMediator _mediator;

        public PanierController(IMediator mediator)
        {
            _mediator = mediator;
        }

        // GET: api/<PanierController>
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var query = new RetourneTousLesPaniersQuery();
            var result = await _mediator.Send(query);
            return Ok(result);
        }

        // GET api/<PanierController>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var query = new RetourneUnPanierParIdQuery(id);
            var result = await _mediator.Send(query);
            return Ok(result);
        }

        // POST api/<PanierController>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] PanierACreerDto panier)
        {
            var query = new CreeNouveauPanierCommand()
            {
                ClientId = panier.ClientId
            };
            var result = await _mediator.Send(query);
            return Ok(result);
        }

        // POST api/<PanierController>/Facture
        [HttpPost("{id}/Facture")]
        public async Task<IActionResult> Facture(int id)
        {
            var query = new RetourneFacturePourPanierQuery(id);
            var result = await _mediator.Send(query);
            return Ok(result);
        }

        // PUT api/<PanierController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] ArticleAAjouteDansLePanierDto article)
        {
            var query = new AjoutArticleAuPanierCommand()
            {
                PanierId = id,
                ArticleId = article.ArticleId,
                Quantite = article.Quantite
            };
            var result = await _mediator.Send(query);
            return Ok(result);
        }

    }
}
