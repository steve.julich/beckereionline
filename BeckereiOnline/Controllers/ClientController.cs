﻿using Beckerei.DomainModel.Clients.Commands;
using Beckerei.DomainModel.Clients.Entities;
using Beckerei.Dtos.Clients;
using Beckerei.Infrastructure.Core;
using Beckerei.ReadDomainModel.Clients.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BeckereiOnline.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClientController : ControllerBase
    {
        private readonly IMediator _mediator;

        public ClientController(IMediator mediator)
        {
            using (var dbContext = new BekereiContext())
            {
                using (var tran1 = dbContext.Database.BeginTransaction())
                {
                    using (var dbContext2 = new BekereiContext())
                    {
                        dbContext2.Database.SetDbConnection(dbContext.Database.GetDbConnection());
                        using (var tran2 = dbContext2.Database.BeginTransaction())
                        {
                            dbContext.Set<Client>().ToList();
                            Console.WriteLine("ici");
                        }
                    }
                }
            }
            _mediator = mediator;
        }

        // GET: api/<ClientController>
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var query = new GetAllClientQuery();
            var result = await _mediator.Send(query);
            return Ok(result);
        }

        // GET api/<ClientController>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var query = new GetOneClientByIdQuery(id);
            var result = await _mediator.Send(query);
            return Ok(result);
        }

        // POST api/<ClientController>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] ClientAAjouteDto client)
        {
            var command = new CreeNouveauClientCommand()
            {
                Nom = client.Nom,
                Prenom = client.Prenom,
                Adresse1 = client.Adresse1,
                Adresse2 = client.Adresse2,
                CodePostal = client.CodePostal,
                Ville = client.Ville
            };
            var result = await _mediator.Send(command);
            return Ok(result);
        }

    }
}
