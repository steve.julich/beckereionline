﻿using Beckerei.DomainModel.Articles.Commands;
using Beckerei.Dtos.Articles;
using Beckerei.ReadDomainModel.Articles.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeckereiOnline.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ArticleController : ControllerBase
    {
        private readonly IMediator _mediator;

        public ArticleController(IMediator mediator)
        {
            _mediator = mediator;
        }

        // GET: api/<ArticleController>
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] bool EnStockUniquement = true)
        {
            var query = new GetAllArticleQuery()
            {
                EnStockUniquement = EnStockUniquement
            };
            var result = await _mediator.Send(query);
            return Ok(result);
        }

        // GET: api/<ArticleController>/searchByName/
        [HttpGet("searchByName/{pattern}")]
        public async Task<IActionResult> SearchByName(string pattern)
        {
            var query = new GetArticleByNameQuery(pattern);
            var result = await _mediator.Send(query);
            return Ok(result);
        }

        // GET api/<ArticleController>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var query = new GetOneArticleByIdQuery(id);
            var result = await _mediator.Send(query);
            return Ok(result);
        }

        // POST api/<ArticleController>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] ArticleAAjouteDto Article)
        {
            var command = new CreeNouvelArticleCommand()
            {
                Nom = Article.Nom,
                Poids = Article.Poids,
                QuantiteEnStock = Article.QuantiteEnStock,
                PrixUnitaire = Article.PrixUnitaire
            };
            var result = await _mediator.Send(command);
            return Ok(result);
        }
    }
}
