﻿using AutoMapper;
using Beckerei.DomainModel.Articles.Repositories;
using Beckerei.Dtos.Articles;
using Beckerei.ReadDomainModel.Articles.Queries;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Beckerei.ReadDomainModel.Articles.QueryHandlers
{
    public class GetOneArticleByIdQueryHandler : IRequestHandler<GetOneArticleByIdQuery, ArticleDetailDto>
    {
        private readonly IMapper _mapper;
        private readonly IRepositoryArticles _articlesRepository;

        public GetOneArticleByIdQueryHandler(IMapper mapper, IRepositoryArticles articlesRepository)
        {
            _mapper = mapper;
            _articlesRepository = articlesRepository;
        }

        public async Task<ArticleDetailDto> Handle(GetOneArticleByIdQuery request, CancellationToken cancellationToken)
        {
            var article = await _articlesRepository.GetArticleByIdAsync(request.ArticleId);
            return _mapper.Map<ArticleDetailDto>(article);
        }
    }
}
