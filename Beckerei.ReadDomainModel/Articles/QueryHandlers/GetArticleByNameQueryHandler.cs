﻿using AutoMapper;
using Beckerei.DomainModel.Articles.Repositories;
using Beckerei.Dtos.Articles;
using Beckerei.ReadDomainModel.Articles.Queries;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Beckerei.ReadDomainModel.Articles.QueryHandlers
{
    public class GetArticleByNameQueryHandler : IRequestHandler<GetArticleByNameQuery, List<ArticleForListDto>>
    {
        private readonly List<string> forbiddenWords = new List<string> { "chocolatine", "rhum" };
        private readonly IRepositoryArticles _articlesRepository;
        private readonly IMapper _mapper;

        public GetArticleByNameQueryHandler(IMapper mapper, IRepositoryArticles articlesRepository)
        {
            _mapper = mapper;
            _articlesRepository = articlesRepository;
        }

        public async Task<List<ArticleForListDto>> Handle(GetArticleByNameQuery request, CancellationToken cancellationToken)
        {
            var articles = await _articlesRepository.GetArticlesByNameAsync(request.Pattern);
            //On exclus les articles contenant un mot interdit dans leur nom
            articles = articles.AsQueryable().Where(a => !forbiddenWords.Any(fb=>fb.Contains(a.Nom.ToLower()))).ToList();
            return _mapper.Map<List<ArticleForListDto>>(articles);
        }
    }
}
