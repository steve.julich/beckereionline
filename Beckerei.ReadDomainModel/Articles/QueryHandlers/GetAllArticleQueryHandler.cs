﻿using AutoMapper;
using Beckerei.DomainModel.Articles.Repositories;
using Beckerei.Dtos.Articles;
using Beckerei.ReadDomainModel.Articles.Queries;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Beckerei.ReadDomainModel.Articles.QueryHandlers
{
    public class GetAllArticleQueryHandler : IRequestHandler<GetAllArticleQuery, List<ArticleForListDto>>
    {
        private readonly IMapper _mapper;
        private readonly IRepositoryArticles _articlesRepository;

        public GetAllArticleQueryHandler(IMapper mapper, IRepositoryArticles articlesRepository)
        {
            _mapper = mapper;
            _articlesRepository = articlesRepository;
        }

        public async Task<List<ArticleForListDto>> Handle(GetAllArticleQuery request, CancellationToken cancellationToken)
        {
            var articles = await _articlesRepository.GetArticlesAsync(request.EnStockUniquement);
            return _mapper.Map<List<ArticleForListDto>>(articles);
        }
    }
}
