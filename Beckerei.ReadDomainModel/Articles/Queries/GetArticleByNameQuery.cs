﻿using Beckerei.Dtos.Articles;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Beckerei.ReadDomainModel.Articles.Queries
{
    public class GetArticleByNameQuery : IRequest<List<ArticleForListDto>>
    {
        public string Pattern { get; private set; }

        public GetArticleByNameQuery(string pattern)
        {
            Pattern = pattern;
        }
    }
}
