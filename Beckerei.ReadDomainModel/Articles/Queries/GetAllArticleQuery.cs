﻿using Beckerei.Dtos.Articles;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Beckerei.ReadDomainModel.Articles.Queries
{
    public class GetAllArticleQuery : IRequest<List<ArticleForListDto>>
    {
        public bool EnStockUniquement { get; set; }
    }
}
