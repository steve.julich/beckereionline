﻿using Beckerei.Dtos.Articles;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Beckerei.ReadDomainModel.Articles.Queries
{
    public class GetOneArticleByIdQuery : IRequest<ArticleDetailDto>
    {
        public int ArticleId { get; private set; }
        public GetOneArticleByIdQuery(int articleId)
        {
            ArticleId = articleId;
        }
    }
}
