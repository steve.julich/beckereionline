﻿using AutoMapper;
using Beckerei.DomainModel.Articles.Entities;
using Beckerei.DomainModel.Clients.Entities;
using Beckerei.DomainModel.Paniers.Entities;
using Beckerei.Dtos.Articles;
using Beckerei.Dtos.Clients;
using Beckerei.Dtos.Paniers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Beckerei.ReadDomainModel.Mappings
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<Panier, PanierForListDto>()
                .ForMember(m => m.Id, opt => opt.MapFrom(s => s.Id))
                .ForMember(m => m.Client, opt => opt.MapFrom(s => $"{s.Client.Nom} {s.Client.Prenom}"))
                .ForMember(m => m.NbArticles, opt => opt.MapFrom(s => s.Articles.Sum(a=>a.Quantite)));

            CreateMap<Panier, PanierDetailDto>();

            CreateMap<ArticleDansLePanier, ArticleDansLePanierDto>();

            CreateMap<Client, ClientDto>()
                .ForMember(m => m.Id, opt => opt.MapFrom(s => s.Id))
                .ForMember(m => m.Nom, opt => opt.MapFrom(s => s.Nom))
                .ForMember(m => m.Prenom, opt => opt.MapFrom(s => s.Prenom));

            CreateMap<Client, ClientDetailDto>();

            CreateMap<Article, ArticleForListDto>()
                .ForMember(m => m.Id, opt => opt.MapFrom(s => s.Id))
                .ForMember(m => m.Nom, opt => opt.MapFrom(s => s.Nom))
                .ForMember(m => m.PrixUnitaire, opt => opt.MapFrom(s => s.PrixUnitaire));

            CreateMap<Article, ArticleDetailDto>();

            CreateMap<ArticleDansLePanier, LigneArticlePourFactureDto>()
                .ForMember(m => m.PoidsTotal, opt => opt.MapFrom(s => s.Quantite * s.Article.Poids))
                .ForMember(m => m.PrixTotal, opt => opt.MapFrom(s => s.Quantite * s.Article.PrixUnitaire))
                .ForMember(m => m.PrixUnitaire, opt => opt.MapFrom(s => s.Article.PrixUnitaire))
                .ForMember(m => m.Quantite, opt => opt.MapFrom(s => s.Quantite))
                .ForMember(m => m.Nom, opt => opt.MapFrom(s => s.Article.Nom));
        }
    }
}
