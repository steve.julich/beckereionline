﻿using AutoMapper;
using Beckerei.DomainModel.Paniers.Repositories;
using Beckerei.Dtos.Paniers;
using Beckerei.ReadDomainModel.Paniers.Queries;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Beckerei.ReadDomainModel.Paniers.QueryHandlers
{
    public class RetourneTousLesPaniersQueryHandler : IRequestHandler<RetourneTousLesPaniersQuery, List<PanierForListDto>>
    {
        private readonly IRepositoryPaniers _paniersRepository;
        private readonly IMapper _mapper;

        public RetourneTousLesPaniersQueryHandler(IRepositoryPaniers paniersRepository, IMapper mapper)
        {
            _paniersRepository = paniersRepository;
            _mapper = mapper;
        }

        public async Task<List<PanierForListDto>> Handle(RetourneTousLesPaniersQuery request, CancellationToken cancellationToken)
        {
            var paniers = await _paniersRepository.GetPanierAsync();
            return _mapper.Map<List<PanierForListDto>>(paniers);
        }
    }
}
