﻿using AutoMapper;
using Beckerei.DomainModel.Paniers.Repositories;
using Beckerei.Dtos.Paniers;
using Beckerei.ReadDomainModel.Paniers.Queries;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Beckerei.ReadDomainModel.Paniers.QueryHandlers
{
    public class RetourneUnPanierParIdQueryHandler : IRequestHandler<RetourneUnPanierParIdQuery, PanierDetailDto>
    {
        private readonly IRepositoryPaniers _paniersRepository;
        private readonly IMapper _mapper;

        public RetourneUnPanierParIdQueryHandler(IRepositoryPaniers paniersRepository, IMapper mapper)
        {
            _paniersRepository = paniersRepository;
            _mapper = mapper;
        }

        public async Task<PanierDetailDto> Handle(RetourneUnPanierParIdQuery request, CancellationToken cancellationToken)
        {
            var panier = await _paniersRepository.GetPanierByIdAsync(request.Id);

            if (panier == null)
                throw new KeyNotFoundException($"Le panier {request.Id} n'existe pas");

            return _mapper.Map<PanierDetailDto>(panier);
        }
    }
}
