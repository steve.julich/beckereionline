﻿using AutoMapper;
using Beckerei.DomainModel.Paniers.Repositories;
using Beckerei.Dtos.Paniers;
using Beckerei.ReadDomainModel.Paniers.Queries;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Beckerei.ReadDomainModel.Paniers.QueryHandlers
{
    public class RetourneFacturePourPanierQueryHandler : IRequestHandler<RetourneFacturePourPanierQuery, FactureDto>
    {
        private readonly IMapper _mapper;
        private readonly IRepositoryPaniers _paniersRepository;

        public RetourneFacturePourPanierQueryHandler(IRepositoryPaniers paniersRepository, IMapper mapper)
        {
            _paniersRepository = paniersRepository;
            _mapper = mapper;
        }

        public async Task<FactureDto> Handle(RetourneFacturePourPanierQuery request, CancellationToken cancellationToken)
        {
            var panier = await _paniersRepository.GetPanierByIdAsync(request.Id);
            var facture = new FactureDto
            {
                NombreArticleTotal = panier.Articles.Sum(a => a.Quantite),
                PoidsTotal = panier.Articles.Sum(a => a.Quantite * a.Article.Poids),
                Prixtotal = panier.Articles.Sum(a => a.Quantite * a.Article.PrixUnitaire),
                Articles = _mapper.Map<List<LigneArticlePourFactureDto>>(panier.Articles)
            };

            return facture;
        }
    }
}
