﻿using Beckerei.Dtos.Paniers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Beckerei.ReadDomainModel.Paniers.Queries
{
    public class RetourneTousLesPaniersQuery : IRequest<List<PanierForListDto>>
    {
    }
}
