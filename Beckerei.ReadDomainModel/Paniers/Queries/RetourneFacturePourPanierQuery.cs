﻿using Beckerei.Dtos.Paniers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Beckerei.ReadDomainModel.Paniers.Queries
{
    public class RetourneFacturePourPanierQuery : IRequest<FactureDto>
    {
        public int Id { get; private set; }
        public RetourneFacturePourPanierQuery(int id)
        {
            Id = id;
        }
    }
}
