﻿using Beckerei.Dtos.Paniers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Beckerei.ReadDomainModel.Paniers.Queries
{
    public class RetourneUnPanierParIdQuery : IRequest<PanierDetailDto>
    {
        public int Id { get; private set; }
        public RetourneUnPanierParIdQuery(int id)
        {
            Id = id;
        }
    }
}
