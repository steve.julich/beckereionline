﻿using AutoMapper;
using Beckerei.DomainModel.Clients.Repositories;
using Beckerei.Dtos.Clients;
using Beckerei.ReadDomainModel.Clients.Queries;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Beckerei.ReadDomainModel.Clients.QueryHandlers
{
    public class GetOneClientByIdQueryHandler : IRequestHandler<GetOneClientByIdQuery, ClientDetailDto>
    {
        private readonly IMapper _mapper;
        private readonly IRepositoryClients _clientsRepository;

        public GetOneClientByIdQueryHandler(IMapper mapper, IRepositoryClients clientsRepository)
        {
            _mapper = mapper;
            _clientsRepository = clientsRepository;
        }

        public async Task<ClientDetailDto> Handle(GetOneClientByIdQuery request, CancellationToken cancellationToken)
        {
            var client = await _clientsRepository.GetClientByIdAsync(request.Id);
            return _mapper.Map<ClientDetailDto>(client);
        }
    }
}
