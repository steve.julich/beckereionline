﻿using AutoMapper;
using Beckerei.DomainModel.Clients.Repositories;
using Beckerei.Dtos.Clients;
using Beckerei.ReadDomainModel.Clients.Queries;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Beckerei.ReadDomainModel.Clients.QueryHandlers
{
    public class GetAllClientQueryHandler : IRequestHandler<GetAllClientQuery, List<ClientDto>>
    {
        private readonly IRepositoryClients _clientsRepository;
        private readonly IMapper _mapper;

        public GetAllClientQueryHandler(IRepositoryClients clientsRepository, IMapper mapper)
        {
            _clientsRepository = clientsRepository;
            _mapper = mapper;
        }

        public async Task<List<ClientDto>> Handle(GetAllClientQuery request, CancellationToken cancellationToken)
        {
            var clients = await _clientsRepository.GetClientsAsync();
            return _mapper.Map<List<ClientDto>>(clients);
        }
    }
}
