﻿using Beckerei.Dtos.Clients;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Beckerei.ReadDomainModel.Clients.Queries
{
    public class GetAllClientQuery : IRequest<List<ClientDto>>
    {
    }
}
