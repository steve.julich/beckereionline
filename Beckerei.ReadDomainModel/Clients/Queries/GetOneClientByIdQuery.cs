﻿using Beckerei.Dtos.Clients;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Beckerei.ReadDomainModel.Clients.Queries
{
    public class GetOneClientByIdQuery : IRequest<ClientDetailDto>
    {
        public int Id { get; private set; }

        public GetOneClientByIdQuery(int id)
        {
            Id = id;
        }
    }
}
